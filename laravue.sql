-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 19, 2018 at 04:40 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravue`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `website`, `created_at`, `updated_at`) VALUES
(1, 'Martine Wiegand', 'metz.birdie@example.com', '613.773.2811 x35912', 'http://bartell.com/atque-dolore-fugiat-amet-veniam', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(2, 'Mrs. Felicia Goldner', 'cathy97@example.org', '1-326-814-3507 x936', 'http://swift.biz/ut-vel-et-cumque-ad-rerum-quod', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(3, 'Prof. Braxton Yost', 'boyer.therese@example.net', '831.436.6076', 'http://weissnat.com/ab-et-omnis-deleniti-ducimus', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(4, 'Dr. Sanford Dach', 'halie.fahey@example.com', '+1-484-382-5937', 'http://zulauf.com/temporibus-eum-sequi-nihil.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(5, 'Verlie Gerhold Sr.', 'goodwin.theodora@example.org', '997-678-6588 x779', 'http://www.hartmann.net/et-porro-dolores-velit-repudiandae-non', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(6, 'Curt King', 'violette12@example.net', '+1 (508) 742-8440', 'http://www.murray.com/et-ad-recusandae-ipsam-autem-aut-libero-sunt.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(7, 'Danika Tromp', 'nbashirian@example.org', '+13704047604', 'http://www.torphy.info/hic-perferendis-culpa-quidem-est-eveniet-unde', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(8, 'Emanuel Block DDS', 'bella.legros@example.org', '758.500.3960 x35968', 'http://hirthe.com/occaecati-eos-et-autem-reprehenderit-repellat-vel-ratione', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(9, 'Prof. Marshall Hartmann', 'buckridge.abelardo@example.org', '334.776.5077', 'http://www.champlin.com/harum-esse-velit-magni-odio', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(10, 'Clarissa Stracke', 'weber.ryley@example.org', '521.367.5089 x1482', 'http://rowe.com/vel-aperiam-facilis-error-vitae', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(11, 'Mr. Aurelio Simonis III', 'mekhi08@example.com', '(850) 875-6853 x95550', 'http://steuber.com/at-similique-illum-quo-corporis-et-commodi-temporibus', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(12, 'Josefina Hahn', 'kub.destini@example.net', '+1.216.772.1756', 'http://hessel.org/natus-consequatur-sit-sunt-nulla-tempora-voluptates-commodi', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(13, 'Joyce Watsica Sr.', 'lavina.cremin@example.com', '993.702.1077 x3024', 'http://www.nienow.org/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(14, 'Tomas Smith', 'dewitt33@example.com', '363-752-0109 x62457', 'http://www.connelly.info/consequatur-natus-voluptatem-omnis-commodi-ab-et-aut-velit', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(15, 'Claudia Hartmann', 'marcella.marquardt@example.com', '1-452-615-8311 x3103', 'http://crona.org/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(16, 'Cameron Greenholt', 'little.cristopher@example.org', '+17173084565', 'https://www.herzog.org/modi-sunt-voluptate-dolorum-quas', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(17, 'Ms. Yazmin Gerhold', 'adonis60@example.com', '636-613-9807 x857', 'https://kassulke.com/sit-quas-id-enim-aliquam.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(18, 'Amaya Padberg', 'botsford.nikita@example.net', '220-487-2360', 'http://kovacek.com/non-temporibus-odio-accusamus-voluptate', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(19, 'Albertha Heaney', 'christiansen.hugh@example.com', '(573) 551-0407 x498', 'http://stamm.com/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(20, 'Eleonore Rolfson', 'dooley.emory@example.com', '1-209-919-8133', 'http://www.vandervort.org/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(21, 'Kendrick Mertz', 'henderson.franecki@example.org', '1-786-202-0137 x714', 'http://www.wiegand.org/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(22, 'Eli Crooks', 'kacey70@example.com', '+1-794-622-3580', 'http://predovic.com/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(23, 'Emie Cummings', 'tristian85@example.net', '+1.395.571.9316', 'http://www.pagac.com/voluptates-ex-accusantium-eos-aut-aut', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(24, 'Era Smitham', 'carson36@example.net', '265-419-1843', 'https://www.schultz.org/omnis-corporis-ex-iusto-est', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(25, 'Lilliana Kuhic', 'tblock@example.org', '(267) 682-0714 x2843', 'http://www.volkman.com/voluptate-blanditiis-cum-voluptatem-pariatur-quia-quo', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(26, 'Lelia Braun III', 'carlos.stroman@example.com', '+1.584.749.4125', 'http://www.kub.com/provident-voluptatem-natus-voluptatum-debitis-et-perspiciatis-impedit-consequatur', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(27, 'Lolita Grant', 'cummerata.brayan@example.com', '423.368.7371 x96471', 'https://leuschke.com/rerum-impedit-consequatur-optio-vitae-dolores-neque.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(28, 'Keanu Huels', 'wbashirian@example.org', '1-763-300-8917 x47590', 'http://www.jones.net/aut-voluptas-blanditiis-earum-dolorem', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(29, 'Genevieve Corwin', 'tia.hessel@example.org', '1-624-391-0488 x716', 'http://www.mcclure.com/natus-animi-accusamus-dolor-inventore', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(30, 'Lamont Kub', 'wbatz@example.net', '696-616-1211 x754', 'http://lesch.com/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(31, 'Aimee Hayes', 'fletcher.schuppe@example.org', '(675) 845-8017 x8340', 'http://www.lind.info/harum-aperiam-aut-consequatur-molestiae', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(32, 'Omer Wilderman', 'jhauck@example.com', '1-369-514-2598', 'http://gaylord.com/sapiente-quas-eaque-eos', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(33, 'Kip Hintz PhD', 'rosanna.zboncak@example.net', '1-213-851-4543 x611', 'http://cruickshank.com/laudantium-minus-assumenda-velit-natus-adipisci.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(34, 'Benedict Weimann', 'layla16@example.com', '393-776-4386', 'http://www.morissette.net/voluptates-et-est-exercitationem-fuga-saepe-est.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(35, 'Brooklyn Romaguera', 'oleta.schmeler@example.net', '351.846.1339 x8988', 'http://kerluke.com/', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(36, 'Christopher Murazik', 'maiya01@example.com', '+16483634970', 'http://www.haag.com/nisi-impedit-autem-porro-reiciendis-ut-id.html', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(37, 'Darian Price', 'carter.sadie@example.org', '735.217.2479 x3622', 'http://www.fahey.com/doloribus-sed-autem-dicta-dolore-non-earum', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(38, 'Sarah Lindgren III', 'dubuque.hubert@example.net', '550-805-2515', 'https://www.hudson.com/a-eum-ut-quis-neque-nisi-similique-voluptas', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(39, 'Mrs. Eugenia Roob PhD', 'moore.brendon@example.com', '+1 (773) 383-0417', 'http://www.rau.com/rerum-nemo-aut-maiores-doloribus-perspiciatis-nisi', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(40, 'Rachelle Pouros', 'hane.royce@example.com', '284.709.8490', 'http://abbott.com/natus-enim-nihil-itaque-atque-accusamus-qui-dolores-nihil', '2018-09-18 17:32:23', '2018-09-18 17:32:23'),
(41, 'Drake Roob', 'breanne.berge@example.org', '+1-994-290-1336', 'http://www.hirthe.biz/quae-cupiditate-est-aperiam-aliquam-dolorem-sit.html', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(42, 'Pete Hirthe', 'nolan.kacey@example.org', '202-889-7621', 'http://stamm.net/', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(43, 'Rosalind Marquardt', 'block.kayli@example.org', '(608) 909-9483', 'http://hoeger.com/officia-tempore-temporibus-magni-voluptatem-similique-fugiat-fugiat.html', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(44, 'Jerrell Thompson', 'kassulke.mathew@example.org', '(449) 318-5326 x233', 'http://www.tremblay.com/', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(45, 'Ally Ryan', 'grimes.ollie@example.com', '(574) 784-4188 x873', 'http://www.dibbert.com/exercitationem-minus-quam-id-aspernatur', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(46, 'Glenda Littel I', 'michele47@example.org', '+1 (754) 341-3145', 'https://parker.com/repellat-repudiandae-tenetur-animi-occaecati-reiciendis-consequatur-sint-eius.html', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(47, 'Milford Schmidt', 'leland01@example.org', '658.223.3633 x5483', 'http://reinger.com/qui-ut-fugiat-eos-magnam-ut.html', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(48, 'Camylle Johnson', 'aconnelly@example.com', '1-498-991-5078', 'http://hill.info/', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(49, 'Marlon Russel', 'itorphy@example.com', '(570) 829-9215 x782', 'http://kessler.com/possimus-sunt-facilis-error-enim-minima-minima', '2018-09-18 17:32:24', '2018-09-18 17:32:24'),
(50, 'Nola Schmidt Sr.', 'gus16@example.net', '(739) 243-3921 x9629', 'http://rutherford.net/sint-libero-voluptatem-dolor-doloremque-perferendis-omnis-nobis', '2018-09-18 17:32:24', '2018-09-18 17:32:24');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_17_005745_create_customers_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Maddison Feil', 'wolf.alia@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '6uGltMwtWQ', '2018-09-15 01:51:48', '2018-09-15 01:51:48'),
(3, 'Rosalind Kautzer Sr.', 'jabari.feest@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'pWlZOMuXDP', '2018-09-15 01:51:48', '2018-09-16 06:32:33'),
(4, 'Grace Hodkiewicz', 'danielle82@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'bWdQcodQvv', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(5, 'Antonia Windler', 'jasmin.ernser@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '4CxvBEpcIQ', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(6, 'Mrs. Diana Lebsack', 'cdamore@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '1PDhd17B96', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(7, 'Gabe Will', 'rath.regan@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'OhW6ArWF8v', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(8, 'Mackenzie Senger', 'xrolfson@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'M0bb98FcxS', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(9, 'Fidel Bauch', 'frankie.runte@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'lcyB2SuAQY', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(10, 'Rusty Rippin', 'alden.quigley@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'WytMizTRv0', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(11, 'Bernard Reichert', 'leffertz@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'QJjECRWtGc', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(12, 'Alfredo Howe', 'vena.senger@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'YzxWiCUAOG', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(13, 'Prof. Camden Tillman II', 'gkautzer@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'DmMPTkmWc8', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(14, 'Armand Dicki', 'langosh.dexter@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'p8sF7E4lf4', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(15, 'Dr. Isaac Haag DDS', 'chad.turner@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'zN8LFCBdpp', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(16, 'Niko Lakin', 'wendell57@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'NQrCnLnkoS', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(17, 'Eli Effertz', 'corwin.cesar@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'LmIMEtAG6s', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(18, 'Rudolph Waelchi', 'goodwin.bella@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'hrXDsQX3p0', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(19, 'Jackson Welch', 'antwan.swift@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'URmSf25r8F', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(20, 'Dr. Lula Hartmann', 'shields.jace@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'vaGbPKVnwS', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(21, 'Billy Larkin', 'lesley.fay@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'QF5uhFSfmI', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(22, 'Mr. Maxime Grady MD', 'raoul57@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'EE8AZYVzFB', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(23, 'Mona Tromp', 'price.eveline@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'KkYp9xLjcY', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(24, 'Kenneth Schoen', 'germaine.kuphal@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'K96dxQqtyP', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(25, 'Miss Andreanne Johnston', 'avis57@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'F4ts7u0Ckq', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(26, 'Wilton Vandervort', 'morris14@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'GWouwo3cEp', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(27, 'Cornell Rau', 'dusty.casper@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'grRl1TmRjs', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(28, 'Mrs. Edythe Bartoletti DVM', 'leonor.mitchell@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'BdMPP3U8p0', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(29, 'Dr. Mariana Robel Sr.', 'ctillman@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'DpuxL8ii9Y', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(30, 'Alfred Crona', 'hudson89@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 's2H0sk6hV6', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(31, 'Lavonne Walsh', 'bernhard.boehm@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'VAsZGcOA63', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(32, 'Prof. Eudora Kulas', 'callie.ritchie@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'xK2U7gJfpR', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(33, 'Prof. Liliane Cummerata', 'sonya.rippin@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'WTSwhdNxRx', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(34, 'Dr. Cooper Cruickshank Jr.', 'lavada61@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'F2ck0QJcWL', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(35, 'Mr. Percival Crooks V', 'thaddeus.friesen@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'HsNYkPIw2D', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(36, 'Dr. Novella Hammes', 'griffin.keebler@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'Z7pQdKSA23', '2018-09-15 01:51:49', '2018-09-15 01:51:49'),
(37, 'Adonis Dooley', 'lynn.cremin@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'fYURDakqMo', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(38, 'Emely Brown', 'norbert.mraz@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'CTFyA6uLcw', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(39, 'Mallory Schmitt', 'madaline.oconner@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'ampQ9j5ICc', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(40, 'Mina Reilly', 'dweimann@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '7R96pFqaax', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(41, 'America Tremblay Sr.', 'dorris.nolan@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '8uG0NCLyOK', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(42, 'Thea Swift II', 'zula.schimmel@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'JC1wYHIAWp', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(43, 'Uriah Hand', 'beahan.carmella@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'mWHkA21O22', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(44, 'Lydia Cremin', 'benedict25@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'ZIvzCHewS0', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(45, 'Zetta Schmeler', 'abeer@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'j5XfcPam6u', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(46, 'Timmy Bradtke', 'rollin96@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'lCr0GbAHTF', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(47, 'Roger Hane', 'vickie.stanton@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '1mGUBuNhCV', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(48, 'Ms. Whitney McKenzie', 'roberto.berge@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'om1huNgwdT', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(49, 'Domenica Corkery', 'winston74@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'CFP8VWAvbI', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(50, 'Prof. Aletha Runolfsdottir DVM', 'homenick.alejandra@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'gqLRdVrqoy', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(51, 'Adrian Leannon', 'carter.curt@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'thagrP68Vz', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(52, 'Madyson Sporer', 'ziemann.ellie@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'RzK8JN9hCq', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(53, 'Mr. Modesto Reynolds IV', 'turner.ratke@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'PUxKirj7om', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(54, 'Pablo Wyman', 'christophe.dicki@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '0p1WNxpQtN', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(55, 'Taurean Koch', 'lorena.crist@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'CImy2idcEX', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(56, 'Dr. Marques Greenfelder II', 'dubuque.lue@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '7mC50OewQe', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(57, 'Morton Frami', 'destany37@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '4PlxEOvSma', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(58, 'Beau Smitham PhD', 'marcel64@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'AS5crTUi19', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(59, 'Ona Hegmann', 'mmraz@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'ualLSgvvy9', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(60, 'Miss Stella Wintheiser Sr.', 'rreynolds@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '1siUQEaJxy', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(61, 'Ian Rath Sr.', 'considine.alize@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'gMXzXmKcRP', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(62, 'Milton Fadel', 'collins.rosetta@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '07inSjqowO', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(63, 'Flavio Johnson', 'derek.cole@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'WSCDLkEueB', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(64, 'Prof. Erna Witting II', 'howell.shana@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'SpqMyBt6ZN', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(65, 'Elvera Auer', 'cauer@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '0it66FBAVd', '2018-09-15 01:51:50', '2018-09-15 01:51:50'),
(66, 'Euna Jast', 'maverick29@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'VGWWy0nf7g', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(67, 'Prof. Kellen Hayes MD', 'sidney.hintz@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'zjpw69qtZJ', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(68, 'Dino Schuppe', 'rempel.hayden@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'Zm0jIPYeWU', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(69, 'Hassan King I', 'bruen.selmer@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'nggo8StjXM', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(70, 'Stone Blanda IV', 'arturo96@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'oQtWRYs5zf', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(71, 'Mrs. Mercedes Kovacek', 'prosacco.jay@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'xF7OYErrzk', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(72, 'Jordyn Runolfsdottir', 'vschimmel@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '8pMcwuTs3m', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(73, 'Mrs. Kendra Macejkovic', 'damore.kasandra@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'BdElk9TTXe', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(74, 'Bryon Flatley', 'kgoodwin@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'dpjbiZcUv2', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(75, 'Eleanora Gutmann', 'pete.cummerata@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'YuuB9qH16Q', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(76, 'Bonnie Bode', 'magali.howe@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'oQD8eHaU6d', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(77, 'Elyssa Hermiston', 'paolo31@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'OepinXsnyx', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(78, 'Chance Kuphal', 'norwood66@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'DXG194nSfV', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(79, 'Ms. Isabell Runolfsson MD', 'kyla.hintz@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'HRZZFJ2cT3', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(80, 'Wendy Bechtelar', 'spinka.jovan@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'wGm4LMmrRF', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(81, 'Jackie Hilpert', 'kling.summer@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'iDm1XqZ1S7', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(82, 'Benedict Stoltenberg', 'anissa11@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'UbZDeNq6B8', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(83, 'Ismael Nicolas IV', 'davis.eldon@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'JIkw5iQtYS', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(84, 'Stewart Denesik', 'juana.moore@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '7EPSI7wHBK', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(85, 'Evangeline Ullrich', 'daija.oberbrunner@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'qNadCJrECt', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(86, 'Gerry Green', 'kade.purdy@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'uN1z0h83Fv', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(87, 'Edwina Witting', 'okon.jodie@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '2PE6X95XwV', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(88, 'Camila Stoltenberg DDS', 'garret.smith@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '0dQSI2srJ4', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(89, 'Dr. Lew Botsford DDS', 'mabel.farrell@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 's7szSN3OCW', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(90, 'Ardith Lowe', 'pcasper@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'cWFahiO1qo', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(91, 'Cesar Nader', 'kayden.runte@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '346az372Ul', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(92, 'Donny Kiehn', 'zulauf.merritt@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'pklmtX1WVN', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(93, 'Dr. Marcella Howell', 'rmayer@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'C6uo2i0REv', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(94, 'Mrs. Wendy Lockman', 'caterina44@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'SaNVJyiQB8', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(95, 'Adah Schinner', 'lane90@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '3FvC550qcX', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(96, 'Mr. Arch Mraz', 'dell13@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'BGISe9JsLn', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(97, 'Gerda Schowalter', 'buckridge.dianna@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'a6OPwc445Q', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(98, 'Bo Stehr', 'howell.owen@example.com', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', '5vIAz4PtOQ', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(99, 'Prof. Reid Waters', 'murphy.trudie@example.org', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'fLQS7NOv5M', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(100, 'Jerry Beahan', 'lraynor@example.net', NULL, '$2y$10$pv6rSBH7C.gvphMjcrYXc.elv9h455Bn2UJEQEX04NR6PSQBPBpy2', 'a5Z0t7ZXjY', '2018-09-15 01:51:51', '2018-09-15 01:51:51'),
(102, 'Fiorenzi', 'fio@gmail.com', NULL, '$2y$10$nl0tWIAU.MeXy3OvBsPDyuiakDLvh.Cpjo3CF9fA4YnGgewJrbcHm', NULL, '2018-09-15 19:11:50', '2018-09-15 19:11:50'),
(103, 'Ari Empatix', 'ari@empatix.com', NULL, '$2y$10$7nRYIDgx.8cKahFrg86M6OU7c3HpnHWYwsFzcBAZb6p/WIlZlEMa.', NULL, '2018-09-16 07:09:28', '2018-09-16 07:09:28'),
(104, 'Susi', 'susi@yahoo.com', NULL, '$2y$10$weB/czV9G3yXuuqZE6/ziuwx3SmSGm4iJ3uYDX0XWh2JYDjZIaIAO', NULL, '2018-09-16 07:11:34', '2018-09-16 07:11:34'),
(105, 'Ariandi Nugraha', 'db_duabelas@yahoo.com', NULL, '$2y$10$Os9ACKGv403nj.QSlfsjX.R1kYagtK5s0/XL76LdKeDAPKJmtdCbq', NULL, '2018-09-16 18:48:30', '2018-09-16 18:48:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
