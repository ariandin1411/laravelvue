import { getLocalUser } from './helpers/auth';

const user = getLocalUser();

export default {
	state : {
		welcomeMessage : 'Welcome to the jungle',
		currentUser: user,
		isLoggedIn: !!user,
		loading: false,
		auth_error: null,
		customers: [],
		customersz: [],
	},
	mutations : {
		login(state){
			state.loading = true,
			state.auth_error = null;
		},
		loginSuccess(state, payload){
			state.auth_error = null;
			state.isLoggedIn = true;
			state.loading = false;
			state.currentUser = Object.assign({}, payload.user, {token: payload.access_token});

			localStorage.setItem("user", JSON.stringify(state.currentUser));
		},
		loginFailed(state, payload){
			state.loading = false,
			state.auth_error = payload.error;
		},
		logout(state){
			localStorage.removeItem('user');
			state.isLoggedIn = false;
			state.currentUser = null;
		},
		updateCustomers(state, payload) {
      state.customersz = payload;
    }
	},
	getters : {
		welcome(state){
			return state.welcomeMessage;
		},

		isLoading(state){
			return state.loading;
		},

		isLoggedIn(state){
			return state.isLoggedIn;
		},

		currentUser(state){
			return state.currentUser;
		},

		authError(state){
			return state.auth_error;
		},

		customers(state){
			return state.customersz;
		}
	},
	actions : {
		login(context){
			context.commit('login');
		},
		getCustomers(context) {
        axios.get('/api/customers')
        .then((response) => {
        		// console.log(response.data.customers);
            context.commit('updateCustomers', response.data.customers);
        })
        .catch((err) => {
        	console.log(err);
        })
    }
	}
}